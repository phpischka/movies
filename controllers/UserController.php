<?php


class UserController {
     /**
     * Action для страницы "Вход на сайт"
     */
    public function actionLogin()
    {
        // Фильтрация пост запроса
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING, true);
        
        // Обработка формы
        if (isset($post)) {
            // Если форма отправлена 
            // Получаем данные из формы
            $options['login'] = $post['login'];
            $options['password'] = $post['password'];

            // Валидация полей
            $errors = self::validate($options);
            
            if ($errors == false) {
                // Проверяем существует ли пользователь
                $userId = User::checkUserData($options);
                
                if ($userId == false) {
                    
                    // Если данные неправильные - показываем ошибку
                    $errors['status'] = 'Неправильные данные для входа на сайт';
                } else {
                    // Если данные правильные, запоминаем пользователя (сессия)

                    User::auth($userId);

                    // Перенаправляем пользователя в закрытую часть - кабинет 
                    header("Location: /movies");
                }
            }
            
        }

        // Подключаем вид
        require_once(ROOT . '/views/user/login.php');
        
        return true;
    }

    /**
     * Удаляем данные о пользователе из сессии
     */
    public function actionLogout()
    {
        // Удаляем информацию о пользователе из сессии
        unset($_SESSION["user"]);
       
        // Перенаправляем пользователя на главную страницу
         header("Location: /");
         
         return true;
    }
    
    public static function validate($options) {
        
       $errors = false;
        
        if (!isset($options['login']) || empty($options['login'])) {
            $errors['login'] = 'this field is required';
        }
        
        if (!isset($options['password']) || empty($options['password'])) {
            $errors['password'] = 'this field is required';
        }
        
        return $errors;
    }
}
