<?php

/**
 * Контроллер DirectorContriller 
 *
 */
class DirectorController extends AdminBase{
    
    /**
     * Action для страницы "Управление Режиссерами"
     */
    public function actionIndex($page = 1) 
    {
        // Проверка доступа
        self::checkAdmin();
        
        $directors = Director::getDirectors($page);
        
        $total = Director::getTotaDirectors();
       
        // Создаем объект Pagination - постраничная навигация
        $pagination = new Pagination($total, $page, Director::SHOW_BY_DEFAULT, 'page-');
        
        //var_dump($directors);
        require_once ROOT . '/views/directors/index.php';
       
        return true;
    }
    
     /**
     * Action для страницы "Добавить Режиссера"
     */
    public function actionCreate() 
    {
        // Проверка доступа
        self::checkAdmin();
        
        // Фильтрация пост запроса
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING, true);
        
        // Обработка формы
        if (isset($post)) {
           //сохранение введенных данных в сессию
           $_SESSION['fields_director'] = $post;
           
            // Если форма отправлена
            // Получаем данные из формы
            $options['name'] = $post['name'];
            
            //Валидация
            $errors = self::validate($options);
            
            if ($errors == false) {
                // Если ошибок нет
                // Добавляем новый Фильм
                $id = Director::create($options);
               
                if ($id) {
                    //Удаление сессии
                    unset($_SESSION['fields_director']);
                   // Перенаправляем пользователя на страницу фильмов
                    header("Location: /directors");
                }
            } 
        }
        
        //Подключаем вид
        require_once ROOT . '/views/directors/add.php';
        
        return true;
    }
    
    /**
     * Action для страницы "Редактировать Режиссера"
    */
    public function actionUpdate($id) 
    {
        // Проверка доступа
        self::checkAdmin();
        
        // Получаем данные о конкретном Режиссере
        $director = Director::getDirectorById($id);
        
        if (!$director) { 
            header("HTTP/1.0 404 Not Found");
            exit();
        }
        
        // Фильтрация пост запроса
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING, true);
        
        // Обработка формы
        if (isset($post)) {
            // Если форма отправлена
            // Получаем данные из формы
            $options['name'] = $post['name'];
            
            //Валидация
            $errors = self::validate($options);
            
            if ($errors == false) {
                // Если ошибок нет
                // Обновляем Режиссера
                if (Director::update($id, $options)) {
                   // Перенаправляем пользователя на страницу Режиссеров
                    header("Location: /directors");
                }
            } 
        }
        
        //Подключаем вид
        require_once ROOT . '/views/directors/edit.php';
        
        return true;
    }
    
    /**
     * Action для страницы "Удалить Режиссера"
     */
    public function actionDelete($id)
    {
       // Проверка доступа
        self::checkAdmin();
        
        Director::delete($id);
        
        // Перенаправляем пользователя на страницу фильмов
        header("Location: /directors");
        
        return true;
    }
    
    public static function validate($options) {
        
       $errors = false;
        
        if (!isset($options['name']) || empty($options['name'])) {
            $errors['name'] = 'this field is required';
        }
        
        return $errors;
    }
}
