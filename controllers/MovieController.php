<?php

/**
 * Контроллер MovieContriller
 */
class MovieController extends AdminBase{
    
    /**
     * Action для страницы "Управление фильмами"
     */
    public function actionIndex($page = 1) 
    {
        // Проверка доступа
        self::checkAdmin();
       
        $movies = Movie::getMovies($page);
        
        $total = Movie::getTotalMovies();
       
        // Создаем объект Pagination - постраничная навигация
        $pagination = new Pagination($total, $page, Movie::SHOW_BY_DEFAULT, 'page-');
        
        require_once ROOT . '/views/movies/index.php';
       
        return true;
    }
    
     /**
     * Action для страницы "Добавить фильм"
     */
    public function actionCreate() 
    {
        // Проверка доступа
        self::checkAdmin();
        
        //Для выпадающего списка
        $directors = Director::getDirectors();
        
        // Фильтрация пост запроса
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING, true);
        
        // Обработка формы
        if (isset($post)) {
           //сохранение введенных данных в сессию
           $_SESSION['fields'] = $post;
           
            // Если форма отправлена
            // Получаем данные из формы
            $options['directorId'] = $post['directorId'];
            $options['name'] = $post['name'];
            $options['description'] = $post['description'];
            $options['releaseDate'] = $post['releaseDate'];
            
            //Валидация
            $errors = self::validate($options);
            
            if ($errors == false) {
                // Если ошибок нет
                // Добавляем новый Фильм
                $id = Movie::create($options);
               
                if ($id) {
                    //Удаление сессии
                    unset($_SESSION['fields']);
                   // Перенаправляем пользователя на страницу фильмов
                    header("Location: /movies");
                }
            } 
        }
        
        //Подключаем вид
        require_once ROOT . '/views/movies/add.php';
        
        return true;
    }
    
    /**
     * Action для страницы "Редактировать Фильм"
    */
    public function actionUpdate($id) 
    {
        // Проверка доступа
        self::checkAdmin();
        
        //Для выпадающего списка
        $directors = Director::getDirectors();
        
        // Получаем данные о конкретном Фильме
        $movie = Movie::getMovieById($id);
        
        if (!$movie) {
            header("HTTP/1.0 404 Not Found");
            exit();
        }
        
        // Фильтрация пост запроса
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING, true);
        
        // Обработка формы
        if (isset($post)) {
            // Если форма отправлена
            // Получаем данные из формы
            $options['directorId'] = $post['directorId'];
            $options['name'] = $post['name'];
            $options['description'] = $post['description'];
            $options['releaseDate'] = $post['releaseDate'];
            
            //Валидация
            $errors = self::validate($options);
            
            if ($errors == false) {
                // Если ошибок нет
                // Обновляем Фильм
                if (Movie::update($id, $options)) {
                   // Перенаправляем пользователя на страницу фильмов
                    header("Location: /movies");
                }
            } 
        }
        
        //Подключаем вид
        require_once ROOT . '/views/movies/edit.php';
        
        return true;
    }
    
    /**
     * Action для страницы "Удалить Фильм"
     */
    public function actionDelete($id)
    {
        // Проверка доступа
        self::checkAdmin();
        
        Movie::delete($id);
        
        // Перенаправляем пользователя на страницу фильмов
        header("Location: /movies");
        
        return true;
    }
    
    public static function validate($options) {
        
       $errors = false;
       
        if (!isset($options['directorId']) || empty($options['directorId'])) {
            $errors['directorId'] = 'this field is required';
        }
        if (!isset($options['name']) || empty($options['name'])) {
            $errors['name'] = 'this field is required';
        }
        if (!isset($options['description']) || empty($options['description'])) {
            $errors['description'] = 'this field is required';
        }
        if (!isset($options['releaseDate']) || empty($options['releaseDate'])) {
            $errors['releaseDate'] = 'this field is required';
        }
        
        return $errors;
    }
}
