<?php

class HomeController extends AdminBase{
    
   public function actionIndex()
   {
       // Проверка доступа
        $auth = self::checkAdmin();
        
       //Подключаем вид
        require_once ROOT . '/views/home.php';
        
        return true;
   }
}
