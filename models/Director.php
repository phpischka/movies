<?php

/**
 * Модель Director для работы с режисерами
 *Получение из БД, вставка в БД
 */
class Director {
    
    // Количество отображаемых режисеров по умолчанию
    const SHOW_BY_DEFAULT = 5;
    /**
     * Получить всех режиссеров
     */
    public static function getDirectors($page = 1) {
        
        $db = DB::getConnection();
        
        $limit = self::SHOW_BY_DEFAULT;
        // Смещение (для запроса)
        $offset = ($page - 1) * self::SHOW_BY_DEFAULT;
        
        // Получение и возврат результатов
        $sql = "SELECT directorId AS id, name FROM directors "
                . "ORDER BY id ASC LIMIT :limit OFFSET :offset";
        
        // Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':limit', $limit, PDO::PARAM_INT);
        $result->bindParam(':offset', $offset, PDO::PARAM_INT);

        // Выполнение коменды
        $result->execute();
        $directorsList = array();
        $i = 0;
        while ($row = $result->fetch()) {
            $directorsList[$i]['id'] = $row['id'];
            $directorsList[$i]['name'] = $row['name'];
            $i++;
        }
        
        return $directorsList;
    }
    
    /**
     * Получить конкретного режиссера
     */
    public static function getDirectorById($id) {
        
        $db = DB::getConnection();
        
        // Получение и возврат результатов
        $sql = "SELECT * FROM directors WHERE directorId = :id";
        // Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(":id", $id, PDO::PARAM_INT);

        // Указываем, что хотим получить данные в виде массива
        $result->setFetchMode(PDO::FETCH_ASSOC);

        // Выполнение коменды
        $result->execute();

        // Получение и возврат результатов
        return $result->fetch();
    }
    
    /**
     * Добавляет новый режиссера
     * @param array $options <p>Массив с информацией о режиссере</p>
     * @return integer <p>id добавленной в таблицу записи</p>
     */
    public static function create($options)
    {
        // Соединение с БД
        $db = DB::getConnection();
        
        // Текст запроса к БД
        $sql = 'INSERT INTO directors (name) VALUES (:name)';

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        
        if ($result->execute()) {
            // Если запрос выполенен успешно, возвращаем id добавленной записи
            return $db->lastInsertId();
        }
        // Иначе возвращаем 0
        return 0;
    }
     /**
     * Редактирует режиссера с заданным id
     * @param integer $id <p>id режиссера</p>
     * @param array $options <p>Массив с информацей о режиссере</p>
     * @return boolean <p>Результат выполнения метода</p>
     */
    public static function update($id, $options)
    {
        // Соединение с БД
        $db = DB::getConnection();
        
        // Текст запроса к БД
        $sql = "UPDATE directors SET name = :name WHERE directorId = :id";
        
        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        return $result->execute();
    }
    /**
     * Удаляет режиссера с указанным id
     * @param integer $id <p>id режиссера</p>
     * @return boolean <p>Результат выполнения метода</p>
     */
    public static function delete($id)
    {
        // Соединение с БД
        $db = DB::getConnection();

        // Текст запроса к БД
        $sql = 'DELETE FROM directors WHERE directorId = :id';

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }
     /**
     * Возвращаем количество режисеров
     * @return integer
     */
    public static function getTotaDirectors()
    {
        // Соединение с БД
        $db = DB::getConnection();

        // Текст запроса к БД
        $result = $db->query('SELECT count(directorId) AS count FROM directors'); 

        // Возвращаем значение count - количество
        $row = $result->fetch();
        return $row['count'];
    }
}
