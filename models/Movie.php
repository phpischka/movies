<?php

/**
 * Модель Movie для работы с фильмами
 *Получение из БД, вставка в БД
 */
class Movie 
{
    // Количество отображаемых фильмов по умолчанию
    const SHOW_BY_DEFAULT = 5;
    
    public static function getMovies($page = 1)
    {    
        $db = DB::getConnection();
        
        $limit = self::SHOW_BY_DEFAULT;
        // Смещение (для запроса)
        $offset = ($page - 1) * self::SHOW_BY_DEFAULT;
        
        // Текст запроса к БД
        $sql = "SELECT movies.movieId AS id, movies.name AS movieName, "
                . "movies.description, movies.releaseDate, "
                . "directors.name AS directorName"
                . " FROM movies "
                . "LEFT JOIN directors "
                . "ON movies.directorId=directors.directorId "
                . "ORDER BY id ASC LIMIT :limit OFFSET :offset";
        
        // Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':limit', $limit, PDO::PARAM_INT);
        $result->bindParam(':offset', $offset, PDO::PARAM_INT);

        // Выполнение коменды
        $result->execute();
        
         // Получение и возврат результатов
        $moviesList = array();
        $i = 0;
        while ($row = $result->fetch()) {
            $moviesList[$i]['id'] = $row['id'];
            $moviesList[$i]['directorName'] = $row['directorName'];
            $moviesList[$i]['movieName'] = $row['movieName'];
            $moviesList[$i]['description'] = $row['description'];
            $moviesList[$i]['releaseDate'] = $row['releaseDate'];
            $i++;
        }
        
        return $moviesList;
    }
    
    public static function getMovieById($id) {
        
        $db = DB::getConnection();
        
        // Получение и возврат результатов
        $sql = "SELECT movies.movieId AS id, movies.name AS movieName, "
                . "movies.description, movies.releaseDate, "
                . "directors.directorId "
                . " FROM movies "
                . "LEFT JOIN directors "
                . "ON movies.directorId=directors.directorId "
                . "WHERE movies.movieId = :id";
        // Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(":id", $id, PDO::PARAM_INT);

        // Указываем, что хотим получить данные в виде массива
        $result->setFetchMode(PDO::FETCH_ASSOC);

        // Выполнение коменды
        $result->execute();

        // Получение и возврат результатов
        return $result->fetch();
    }
    
    /**
     * Добавляет новый фильм
     * @param array $options <p>Массив с информацией о фильме</p>
     * @return integer <p>id добавленной в таблицу записи</p>
     */
    public static function create($options)
    {
        // Соединение с БД
        $db = DB::getConnection();
        
        // Текст запроса к БД
        $sql = 'INSERT INTO movies '
                . '(directorId, name, description, releaseDate)'
                . 'VALUES '
                . '(:directorId, :name, :description, :releaseDate)';

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':directorId', $options['directorId'], PDO::PARAM_INT);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':description', $options['description'], PDO::PARAM_STR);
        $result->bindParam(':releaseDate', $options['releaseDate'], PDO::PARAM_STR);
        if ($result->execute()) {
            // Если запрос выполенен успешно, возвращаем id добавленной записи
            return $db->lastInsertId();
        }
        // Иначе возвращаем 0
        return 0;
    }
     /**
     * Редактирует фильм с заданным id
     * @param integer $id <p>id фильма</p>
     * @param array $options <p>Массив с информацей о фильме</p>
     * @return boolean <p>Результат выполнения метода</p>
     */
    public static function update($id, $options)
    {
        // Соединение с БД
        $db = DB::getConnection();
        
        // Текст запроса к БД
        $sql = "UPDATE movies SET directorId = :directorId, name = :name, description = :description, releaseDate = :releaseDate WHERE movieId = :id";

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':directorId', $options['directorId'], PDO::PARAM_INT);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':description', $options['description'], PDO::PARAM_STR);
        $result->bindParam(':releaseDate', $options['releaseDate'], PDO::PARAM_STR);
        return $result->execute();
    }
    /**
     * Удаляет фильм с указанным id
     * @param integer $id <p>id фильма</p>
     * @return boolean <p>Результат выполнения метода</p>
     */
    public static function delete($id)
    {
        // Соединение с БД
        $db = DB::getConnection();

        // Текст запроса к БД
        $sql = 'DELETE FROM movies WHERE movieId = :id';

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }
    /**
     * Возвращаем количество фильмов
     * @return integer
     */
    public static function getTotalMovies()
    {
        // Соединение с БД
        $db = DB::getConnection();

        // Текст запроса к БД
        $result = $db->query('SELECT count(movieId) AS count FROM movies'); 

        // Возвращаем значение count - количество
        $row = $result->fetch();
        return $row['count'];
    }
}
