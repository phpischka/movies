<!DOCTYPE html>
<html lang="en">
    <head>
        <title>СoolMovies</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="/template/css/bootstrap.min.css" rel="stylesheet">
        <link href="/template/css/style.css" rel="stylesheet">
    </head>
    <body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="/">
                    СoolMovies
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <?php $request = filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_STRING); ?>
                    
                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link <?php echo $request == '/movies' ?  'current' : '' ?>" href="/movies">Movies</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?php echo $request == '/directors' ?  'current' : '' ?>" href="/directors">Directors</a>
                        </li>
                        
                        <!-- Authentication Links -->
                        <?php if (User::isGuest()): ?>
                            <li class="nav-item">
                                <a class="nav-link" href="login">Login</a>
                            </li>
                        <?php else: ?>
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link" href="#" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    Logout <span class="caret"></span>
                                </a>
                                <form id="logout-form" action="/logout" method="POST" style="display: none;"></form>
                            </li> 
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </nav>