
<?php include ROOT . '/views/layouts/header.php'; ?>

        <main class="py-4">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-5">
                        <div class="card">
                            <div class="card-header">HOME PAGE</div>
                            <div class="card-body">
                                <div class="alert alert-success" role="alert">
                                    Фильм (англ. film — плёнка), а также — кино, кинофильм, телефильм, кинокартина — отдельное произведение киноискусства. В технологическом плане фильм представляет собой совокупность движущихся изображений (монтажных кадров), связанных единым сюжетом[1]. Каждый монтажный кадр состоит из последовательности фотографических или цифровых неподвижных изображений (кадриков), на которых зафиксированы отдельные фазы движения. Фильм, как правило, имеет звуковое сопровождение.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>     
    
<?php include ROOT . '/views/layouts/footer.php'; ?>


